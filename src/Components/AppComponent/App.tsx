import React, {Component} from 'react';
import FormComponent from "../FormComponent/FormComponent";
import ArtistComponent from "../ArtistComponent/ArtistComponent";
import {connect} from "react-redux";


class App extends Component<IAppProps, any> {
    render() {
        return (
            <div className="App">
                <h1>Artist Finder</h1>
                <p>Type in your favorite Artist's name to see the details</p>
                <FormComponent/>
                {this.props.status ? <ArtistComponent/> : ''}

            </div>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        status: state.form.status
    }
};


export default connect(mapStateToProps, {})(App);
