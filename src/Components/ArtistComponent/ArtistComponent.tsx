import React, {Component} from 'react';
import {connect} from "react-redux";
import {store} from '../../Store';
import {GetEventsAction} from "../../Store/Actions/GetEventsAction";
import EventsListComponent from "../Events/EventsListComponent";

class ArtistComponent extends Component<IArtistProps, IArtistState> {
    constructor(props: any) {
        super(props);
        this.getEvents = this.getEvents.bind(this);
    }

    getEvents() {
        if(!this.props.events.requestDone){
            const name = this.props.name;
            store.dispatch<any>(GetEventsAction(name))
        }
    }

    render() {
        const {details} = this.props;
        return (
            <div className="row">
                <div className="card">
                    <img src={details.image_url} alt={details.name}/>
                    <p>{details.name}
                        <br/>
                        <a href={details.facebook_page_url} target="_blank" rel="noopener noreferrer">Facebook Page</a>
                        <br/>
                        <button type="button" onClick={this.getEvents} className="see-events">See {details.name}'s Events</button>
                    </p>

                </div>
                <EventsListComponent />
            </div>

        )
    }
}

const mapStateToProps = (state:IRoot) => {
    return {
        details: state.artistDetails,
        name: state.form.artist,
        events: state.events
    }
};
export default connect(mapStateToProps, {})(ArtistComponent);