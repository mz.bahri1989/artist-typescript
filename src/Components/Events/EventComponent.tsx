import React from 'react';

const EventComponent = (props: IEventProps)=> {
    const {name, city, country, date} = props;
    return (
        <li>
            {name}
            <br/>
            {city}
            <br/>
            {country}
            <br/>
            {date}
        </li>
    )
};

export default EventComponent;
