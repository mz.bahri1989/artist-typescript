import React, { Component } from 'react';
import {connect} from "react-redux";
import {store} from '../../Store';
import {FormChangeAction} from "../../Store/Actions/FormChangeAction";
import {SubmitFormAction} from "../../Store/Actions/SubmitFormAction";
import {ToggleLoadingAction} from "../../Store/Actions/ToggleLoadingAction";

class FormComponent extends Component<IFormProps,any>{
    constructor(props: any){
        super(props);
        this.submitForm = this.submitForm.bind(this);
        this.updateName = this.updateName.bind(this);
    }

    updateName(event:any) {
        store.dispatch(FormChangeAction(event.target.value))
    }



    submitForm(event:any){
        event.preventDefault();
        console.log('form is submitted');
        store.dispatch<any>(ToggleLoadingAction(this.props.form.loading));
        store.dispatch<any>(SubmitFormAction(this.props.form.artist))
    }
    render(){
        const {artist, loading, noArtist, err} = this.props.form;
        return(
            <div className="wrapper">
                <form method="post" action="" onSubmit={this.submitForm}>
                    <input type="text" placeholder="Enter the name of the artist . . ."
                           value={artist}
                           onChange={this.updateName}
                    />
                    <input type="submit" value="submit" />
                </form>
                <img src="./loading.gif" className={`loading' ${loading ? 'show':''}`} alt="loading icon"/>
                <p className={`err ${noArtist ? 'show':''}`}>No such artist. Try again.</p>
                {err ? <p className="err">An error occurred. Please Try again</p>:''}
            </div>

        )
    }
}
const mapStateToProps = (state:IRoot) => {
    return{
        form: state.form
    }
};
export default connect(mapStateToProps,{})(FormComponent);