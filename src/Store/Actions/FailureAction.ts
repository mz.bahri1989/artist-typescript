import {_ERR} from "../types";

export function FailureAction(payload:any):Action{
    return {
        type: _ERR,
        payload
    }
}