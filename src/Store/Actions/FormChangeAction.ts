import {_NAME_CHANGED} from "../types";

export function FormChangeAction(payload: string): Action{
    return{
        type:_NAME_CHANGED,
        payload
    }
}