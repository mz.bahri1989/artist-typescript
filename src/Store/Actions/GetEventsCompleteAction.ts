import {_EVENTS} from "../types";

export function GetEventsCompleteAction(payload:Events):Action{
    return {
        type: _EVENTS,
        payload
    }
}