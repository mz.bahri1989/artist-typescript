import {_NEW_ARTIST} from "../types";

export function SearchCompleteAction(payload: Artist):Action{
    return {
        type: _NEW_ARTIST,
        payload
    }
}