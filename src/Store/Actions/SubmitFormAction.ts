
import axios from "axios";
import {SearchCompleteAction} from './SearchCompleteAction';
import {FailureAction} from './FailureAction';
import {NoArtistAction} from './NoArtistAction';
import {_APPID, _URL} from "../../constants";

export function SubmitFormAction(payload: string) {
    return ((dispatch: any) => {
        axios({
            method: 'GET',
            url: `${_URL}${payload}?app_id=${_APPID}`,
        }).then((res: {data: any}) => {
            if (res.data.error) {
                return dispatch(NoArtistAction());

            }
            return dispatch(SearchCompleteAction(res.data))
        }).catch((err:any) => {
            return dispatch(FailureAction(err))
        })
    })
}