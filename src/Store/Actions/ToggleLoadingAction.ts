import {_SENDING} from "../types";

export function ToggleLoadingAction(payload:boolean):Action{
    return {
        type:_SENDING,
        payload:!payload
    }
}