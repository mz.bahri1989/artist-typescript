import {_NAME_CHANGED, _NEW_ARTIST} from "../types";

const INITIAL_STATE = {
    id:'',
    name:'',
    url:'',
image_url:'',
    thumb_url:'',
    facebook_page_url:'',
    mbid:'',
tracker_count:'',
upcoming_event_count:''
};

export function ArtistReducer(state: Artist = INITIAL_STATE,action:Action){
    switch(action.type){
        case _NAME_CHANGED:
            return INITIAL_STATE;
        case _NEW_ARTIST:
            state = action.payload;
            return state;
        default:
            return state;

    }
}