import {_EVENTS, _NAME_CHANGED} from "../types";

const INITIAL_STATE = {
    requestDone: false,
    eventList:[]
};

export function EventsReducer(state: Events = INITIAL_STATE, action:Action) {
    switch (action.type) {
        case _NAME_CHANGED:

            return {...state, eventList:[],requestDone: false};
        case _EVENTS:
             return {...state, eventList: action.payload, requestDone: true};
        default:
            return state;

    }
}