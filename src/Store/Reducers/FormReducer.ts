import {_ERR, _NAME_CHANGED, _NEW_ARTIST, _NO_ARTIST, _SENDING} from "../types";

const INITIAL_STATE = {
    artist:'',
    status: false,
    loading: false,
    noArtist: false,
    err: false
};
export function FormReducer(state: Form = INITIAL_STATE, action:Action) {
    switch(action.type){
        case _NAME_CHANGED:
            return {...state,artist: action.payload, status: false, noArtist:false};
        case _SENDING:
            return{...state, loading:true};
        case _NO_ARTIST:
            return {...state,loading:false, status: false, noArtist: true};
        case _NEW_ARTIST:
            return {...state,status: true,loading: false, noArtist:false};
        case _ERR:
            return {...state, loading: false, err: true};
        default:
            return state;
    }
}