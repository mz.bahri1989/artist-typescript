import { combineReducers } from 'redux';
import {FormReducer} from "./FormReducer";
import {ArtistReducer} from "./ArtistReducer";
import {EventsReducer} from "./EventsReducer";

export const root = combineReducers({
    form: FormReducer,
    artistDetails: ArtistReducer,
    events: EventsReducer
});