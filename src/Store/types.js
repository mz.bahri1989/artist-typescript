export const _NAME_CHANGED = 'name changed';
export const _SENDING = 'sending request';
export const _NEW_ARTIST = 'new artist data achieved';
export const _EVENTS = 'events list achieved';
export const _NO_ARTIST = "no such artist";
export const _ERR = 'error';