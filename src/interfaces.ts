interface  Action {
    type:string;
    payload:any;
}
interface Form {
    artist:string;
    status: boolean;
    loading: boolean;
    noArtist: boolean;
    err: boolean;
}
interface Artist {
    id:number|string;
    name:string;
    url:string;
    image_url:string;
    thumb_url:string;
    facebook_page_url:string;
    mbid:string;
    tracker_count:number|string;
    upcoming_event_count:string;
}

interface Events {
    requestDone: boolean;
    eventList:Array<EventItem>
}
interface EventItem {
    venue:IEventProps;
    datetime: string;
}
interface IRoot {
    form: Form;
    artistDetails: Artist;
    events: Events;
}


interface IAppProps {
    status: boolean
}

interface IArtistProps{
    events: Events;
    name: string;
    details:Artist;
}
interface IArtistState {
    details: Artist;
    name: string;
    events: Events;
}
interface IEventsProps {
    events: Events;
    name: string;
}
interface IEventProps {
    name:string;
    city:string;
    country:string;
    date:string;
}
interface IFormProps {
    form: Form
}