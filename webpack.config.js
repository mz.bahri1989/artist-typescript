const path = require('path');
module.exports = {
    mode: "production",
    entry: {
        index: './src/index.tsx'
    },
    output: {
        filename: "bundle.js",
        path: path.join(__dirname, "/dist"),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.tsx', '.ts'],
        alias: {
            components: path.resolve(__dirname, '..', './src/components'),
        }
    },

    devServer: {
        contentBase: './dist'
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.ts(x)?$/,
                use: [
                    'awesome-typescript-loader'
                ],
                exclude: /node_modules/
            },

            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader', {
                        loader: "postcss-loader",
                        options: {
                            plugins: () => [require("autoprefixer")({grid: true}),
                                require('cssnano')({preset: 'default'})
                            ],
                            minimize: true
                        },
                    }, {
                        loader: 'sass-loader'
                    }]
            },
            {
                test: /\.(ttf|eot|otf|svg|png|jpg)$/,
                loader: 'file-loader'
            },
            {
                test: /\.(woff|woff2)$/,
                loader: 'url-loader'
            }
        ],

    }
};